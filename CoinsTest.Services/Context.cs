﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoinsTest.Services
{
    public class Context
    {
        private Strategy _strategy;


        public Context(Strategy strategy)
        {
            _strategy = strategy;
        }

        public int[] MakeChange(int[] listOfCoins, int amount)
        {
            return _strategy.MakeChange(listOfCoins, amount);
        }

    }//class
}
