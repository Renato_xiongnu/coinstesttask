﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoinsTest.Services
{
    public class DynamicStrategy: Strategy
    {

        public override int[] MakeChange(int[] listOfCoins, int amount)
        {
            int[] coinUsed = new int[amount + 1];
            int[] coinCount = new int[amount + 1];
            
            int numberOfCoinsForChange = CalcChange(listOfCoins, amount, coinCount, coinUsed);

            int[] change = PrintCoins(coinUsed, amount);

            return change;
        }


        #region "Private Methods"

        private int CalcChange(int[] coinValueList, int change, int[] minCoins, int[] coinsUsed)
        {
            for (int cents = 0; cents < change + 1; cents++)
            {
                int coinCount = cents;
                int newCoin = 1;


                foreach (var j in coinValueList.Where(p => p <= cents))
                {
                    if (minCoins[cents - j] + 1 < coinCount)
                    {
                        coinCount = minCoins[cents - j] + 1;
                        newCoin = j;
                    }//if

                }

                minCoins[cents] = coinCount;
                coinsUsed[cents] = newCoin;
            }//for

            return minCoins[change];
        }


        private int[] PrintCoins(int[] coinsUsed, int change)
        {
            var list = new List<int>();
            int coin = change;

            while (coin > 0)
            {
                int thisCoin = coinsUsed[coin];
                list.Add(thisCoin);
                coin = coin - thisCoin;
            }

            return list.ToArray();
        }
        #endregion

    }
}
