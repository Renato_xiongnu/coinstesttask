﻿using CoinsTest.ViewModel.ViewModelHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace CoinsTest
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            CoinsTest.MainWindow window = new MainWindow();

            window.DataContext = new ModelHub();
            window.Show();
        }

    }
}
