﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace CoinsTest.ViewModel.Extensions
{
    public static class CollectionsHelper
    {
        public static ObservableCollection<T> ToObservableCollectionFromIEnumerable<T>(this IEnumerable<T> col)
        {
            return new ObservableCollection<T>(col);
        }

    }
}
