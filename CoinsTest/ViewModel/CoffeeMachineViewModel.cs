﻿using CoinsTest.Commands;
using CoinsTest.Model;
using CoinsTest.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using CoinsTest.ViewModel.ViewModelHelper;

namespace CoinsTest.ViewModel
{
    public class CoffeeMachineViewModel: ViewModelBase
    {
        private ObservableCollection<Coin> _clientWallet;
        private ObservableCollection<Coin> _machineWallet;
        private ObservableCollection<Product> _products;
        private ObservableCollection<Coin> _virtualWallet;

        public CoffeeMachineViewModel()
        {
            _clientWallet = new ObservableCollection<Coin>();
            _machineWallet = new ObservableCollection<Coin>();
            _products = new ObservableCollection<Product>();
            _virtualWallet = new ObservableCollection<Coin>();

            context = new Context(new DynamicStrategy());

            Inititialize();
        }

        public Context context { get; set; }

        public ObservableCollection<Coin> ClientWallet
        {
            get
            {
                return _clientWallet;
            }
            set
            {        
                _clientWallet = value;
            }
        }

        public ObservableCollection<Coin> MachineWallet
        {
            get
            {
                return _machineWallet;
            }
            set
            {
                _machineWallet = value;
            }
        }

        public ObservableCollection<Product> Products
        {
            get
            {
                return _products;
            }
            set
            {
                _products = value;
            }
        }

        public ObservableCollection<Coin> VirtualWallet
        {
            get
            {
                return _virtualWallet;
            }
            set
            {
                _virtualWallet = value;
            }
        }


         
        private Coin _clientWalletSelectedItem;
        public Coin ClientWalletSelectedItem
        {
            get
            {
                return _clientWalletSelectedItem;
            }
            set
            {
                if (_clientWalletSelectedItem != value)
                {
                    _clientWalletSelectedItem = value;
                    OnPropertyChanged("ClientWalletSelectedItem");        
                }
            }
        }


        private Product _productSelectedItem;
        public Product ProductSelectedItem
        {
            get
            {
                return _productSelectedItem;
            }
            set
            {
                if (_productSelectedItem != value)
                {
                    _productSelectedItem = value;
                    OnPropertyChanged("ProductSelectedItem");
                }
            }
        }


        private void Inititialize()
        {
            //Client's Wallet
            ClientWallet.Add(new Coin()
            {
                 Name = "One",
                 Value = 1,
                 Quantity = 10
            });
            ClientWallet.Add(new Coin()
            {
                Name = "Two",
                Value = 2,
                Quantity = 30
            });
            ClientWallet.Add(new Coin()
            {
                Name = "Five",
                Value = 5,
                Quantity = 20
            });
            ClientWallet.Add(new Coin()
            {
                Name = "Ten",
                Value = 10,
                Quantity = 15
            });

            //Machine's Wallet:
            MachineWallet.Add(new Coin()
            {
                Name = "One",
                Value = 1,
                Quantity = 100
            });
            MachineWallet.Add(new Coin()
            {
                Name = "Two",
                Value = 2,
                Quantity = 100
            });
            MachineWallet.Add(new Coin()
            {
                Name = "Five",
                Value = 5,
                Quantity = 100
            });
            MachineWallet.Add(new Coin()
            {
                Name = "Ten",
                Value = 10,
                Quantity = 100
            });

            //Products:
            Products.Add(new Product()
            {
                ProductName = "Tea",
                Price = 13,
                Quantity = 10
            });
            Products.Add(new Product()
            {
                ProductName = "Coffee",
                Price = 18,
                Quantity = 20
            });
            Products.Add(new Product()
            {
                ProductName = "CoffeeMachiato",
                Price = 21,
                Quantity = 20
            });
            Products.Add(new Product()
            {
                ProductName = "Juce",
                Price = 35,
                Quantity = 15
            });

        }



        #region Commands

 
        private DelegateCommand _throwCoinCommand;
        private DelegateCommand _withdrawCoinsCommand;
        private DelegateCommand _buyProductCommand;


        public ICommand ThrowCoinCommand
        {
            get
            {
                if (_throwCoinCommand == null)
                    _throwCoinCommand = new DelegateCommand(new Action(ThrowCoinExecuted), new Func<bool>(ThrowCoinCanExecute));

                return _throwCoinCommand;
            }
        }

        public ICommand WithdrawCoinsCommand
        {
            get
            {
                if (_withdrawCoinsCommand == null)
                    _withdrawCoinsCommand = new DelegateCommand(new Action(WithdrawCoinExecuted), new Func<bool>(WithdrawCoinCanExecute));

                return _withdrawCoinsCommand;
            }
        }


        public ICommand BuyProductCommand
        {
            get
            {
                if (_buyProductCommand == null)
                    _buyProductCommand = new DelegateCommand(new Action(BuyProductExecuted), new Func<bool>(BuyProductCanExecute));

                return _buyProductCommand;
            }
        }


        //Can execute

        public bool ThrowCoinCanExecute()
        {
            return this.ClientWalletSelectedItem != null; 
        }

 
        public bool WithdrawCoinCanExecute()
        {
            return this.VirtualWallet.Count > 0;
        }

        
        public bool BuyProductCanExecute()
        {
            return this.ProductSelectedItem != null; 
        }
  
        //Execute:

        public void ThrowCoinExecuted()
        {
            this.ClientWalletSelectedItem.Quantity -= 1;

            var vw = this.VirtualWallet.FirstOrDefault(p => p.Name.Equals(this.ClientWalletSelectedItem.Name));

            if (vw == null)
            {
                this.VirtualWallet.Add(new Coin() {
                    Name = this.ClientWalletSelectedItem.Name,
                    Value = this.ClientWalletSelectedItem.Value,
                    Quantity = 1
                });
            }
            else
            {
                vw.Quantity += 1;
            }
        }


        private int[] GetChange(int[] clist)
        {
            int amnt = CoinsHelper.GetSum(this.VirtualWallet);            
            var coinsForChange = context.MakeChange(clist, amnt);

            return coinsForChange;
        }

 

        public void WithdrawCoinExecuted()
        {
            int[] clist = CoinsHelper.GetArrayOfUsedCoins(this.MachineWallet);
            int missingCoin = -1;
            int[] coinsForChange;

            while(missingCoin !=0)
            {
                coinsForChange = GetChange(clist);

                //check if Machine wallet has enough coins
                missingCoin = CoinsHelper.HasMachineWalletEnoughCoins(coinsForChange, this.MachineWallet);

                if (missingCoin != 0)
                {
                    clist = CoinsHelper.RemoveItemFromArray(clist, missingCoin);
                }
                else
                {
                    missingCoin = 0;
                    CoinsHelper.PutCoinsIntoWallet(this.ClientWallet, coinsForChange, this.MachineWallet, this.VirtualWallet);
                }
            } 
        }


        public void BuyProductExecuted()
        {
            var vwSum = this.VirtualWallet.Sum(p=>p.Quantity * p.Value);

            if (this.ProductSelectedItem.Quantity > 0 && vwSum >= this.ProductSelectedItem.Price)
            {
                this.ProductSelectedItem.Quantity -= 1;

                if (vwSum == this.ProductSelectedItem.Price)
                {
                    int[] clist = CoinsHelper.GetArrayOfUsedCoins(this.VirtualWallet);
                    var coinsForChange = context.MakeChange(clist, this.ProductSelectedItem.Price);
                    CoinsHelper.Pay(coinsForChange, this.MachineWallet, this.VirtualWallet);
                }
                else
                {
                    int exchange = vwSum - this.ProductSelectedItem.Price;
                    int[] clist = CoinsHelper.GetArrayOfUsedCoins(this.MachineWallet);
                    var coinsForChange = context.MakeChange(clist, exchange);

                    CoinsHelper.PayWithChange(coinsForChange, this.MachineWallet, this.VirtualWallet);
                }

                MessageBox.Show(string.Format("Thanks, you bought {0}", this.ProductSelectedItem.ProductName));
            }
            else
            {
                MessageBox.Show("Not enough money or product is over");
            }
            
        }


        #endregion


    }//class
}
