﻿using CoinsTest.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoinsTest.ViewModel.ViewModelHelper
{
    public static class CoinsHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="machineWallet"></param>
        /// <returns></returns>
        public static int[] GetArrayOfUsedCoins(ObservableCollection<Coin> machineWallet) 
        {
            var list = new List<int>();

            foreach (var coin in machineWallet)
            {
                if (coin.Quantity > 0)
                {
                    list.Add(coin.Value);
                }
            }

            return list.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="wallet"></param>
        /// <returns></returns>
        public static int GetSum(ObservableCollection<Coin> wallet)
        {
            return wallet.Sum(p => p.Value * p.Quantity);
        }

        
        /// <summary>
        ///Get 0 if coins are sufficient or get another number if it misses some coin equal to this number
        /// </summary>
        /// <param name="change"></param>
        /// <param name="machineWallet"></param>
        /// <returns></returns>
        public static int HasMachineWalletEnoughCoins(int[] change, ObservableCollection<Coin> machineWallet)
        {
            var groups = change.GroupBy(p => p).Select(p => p.ToList()).ToList();
            
            foreach (var group in groups)
            {
                int count = group.Count;
                int val = group[0];

                var coin = machineWallet.FirstOrDefault(p=>p.Value==val);
                if (coin == null)
                {
                    return val;
                }
                else
                {
                    if (coin.Quantity < count)
                    {
                        return val;
                    }
                }
            }

            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientWallet"></param>
        /// <param name="change"></param>
        /// <param name="machineWallet"></param>
        /// <param name="virtualWallet"></param>
        public static void PutCoinsIntoWallet(  ObservableCollection<Coin> clientWallet, 
                                                int[] change,  
                                                ObservableCollection<Coin> machineWallet,
                                                ObservableCollection<Coin> virtualWallet)
        {
            foreach (var coin in change)
            {
                //debit from machine's wallet
                var coinFromMachine = machineWallet.FirstOrDefault(p=>p.Value==coin);
                coinFromMachine.Quantity -= 1;

                //credit to client's wallet
                var coinFromClient = clientWallet.FirstOrDefault(p => p.Value == coin);
                
                if (coinFromClient == null)
                {
                    clientWallet.Add(new Coin() {
                        Name = coinFromMachine.Name,
                        Value = coinFromMachine.Value,
                        Quantity = 1
                    });
                }
                else
                {
                    coinFromClient.Quantity += 1;
                }
            }//foreach

            //move from VirtualWallet to MachineWallet and clean VW:
            foreach (var coin in virtualWallet)
            {
                var coinFromMW = machineWallet.FirstOrDefault(p=>p.Value==coin.Value);
                if (coinFromMW == null)
                {
                    machineWallet.Add(new Coin()
                    {
                        Name = coin.Name,
                        Value = coin.Value,
                        Quantity = coin.Quantity
                    });
                }
                else
                {
                    coinFromMW.Quantity += coin.Quantity;
                }
            }//foreach
            virtualWallet.Clear();
        }

      
        /// <summary>
        /// 
        /// </summary>
        /// <param name="change"></param>
        /// <param name="machineWallet"></param>
        /// <param name="virtualWallet"></param>
        public static void Pay( int[] change,
                                ObservableCollection<Coin> machineWallet,
                                ObservableCollection<Coin> virtualWallet)
        {
            foreach (var coin in change)
            {
                var coinFromVW = virtualWallet.FirstOrDefault(p => p.Value == coin);
                var coinFromMW = machineWallet.FirstOrDefault(p => p.Value == coin);

                if (coinFromVW != null)
                {
                    coinFromVW.Quantity -= 1;

                    if (coinFromVW.Quantity == 0)
                    {
                        virtualWallet.Remove(coinFromVW);
                    }

                    coinFromMW.Quantity += 1;
                }
            }//foreach
        }

  
        /// <summary>
        /// 
        /// </summary>
        /// <param name="change"></param>
        /// <param name="machineWallet"></param>
        /// <param name="virtualWallet"></param>
        public static void PayWithChange(int[] change,
                        ObservableCollection<Coin> machineWallet,
                        ObservableCollection<Coin> virtualWallet)
        {
            //deduct
            foreach (var coin in virtualWallet)
            {
                var coinFromVW = virtualWallet.FirstOrDefault(p => p.Value == coin.Value);                
                var coinFromMW = machineWallet.FirstOrDefault(p => p.Value == coin.Value);
                coinFromMW.Quantity += coin.Quantity;
                coinFromVW.Quantity = 0;
            }
            virtualWallet.Clear();
 

            //return exchange:
            foreach (var coin in change)
            {
                var coinFromVW = virtualWallet.FirstOrDefault(p => p.Value == coin);
                var coinFromMW = machineWallet.FirstOrDefault(p => p.Value == coin);


                coinFromMW.Quantity -= 1;


                if (coinFromVW == null)
                {
                    virtualWallet.Add(new Coin()
                    {
                        Name = coinFromMW.Name,
                        Value = coinFromMW.Value,
                        Quantity = 1
                    });                    
                }
                else
                {
                    coinFromVW.Quantity += 1;
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static int[] RemoveItemFromArray(int[] arr, int item)
        {
            return arr.Where(p => p != item).ToArray();        
        }

    }//class
}
